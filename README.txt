
Description:

This module is a simple helper module to allow users to upload their Geogebra files to their Drupal site, and embed applets in their body of their nodes to view these Geogebra files they have created.  Visit www.geogebra.org for information on documentation of Geogebra.

Warnings:

1.  This module should only currently work with public downloads and clean-urls enabled.
2.  If you have the paths incorrect for some reason, the current version of the Java.com plugin (especially for Netscape/Firefox) will hang your browser.  Make sure you do your testing when you have nothing super important running in the background.

